package net.test.geospatial

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import scala.collection.mutable.ListBuffer
import scala.math._
import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._
import scala.collection.mutable

object ComputeEntropy {
  def main(args: Array[String]): Unit = {
      System.setProperty("hadoop.home.dir", "C:\\winutils")
      
      //Creating Spark Configuration Object
      val conf = new SparkConf()
      .setAppName("ComputeEntropy")
      .setMaster("local")
      
      //Creating Spark Context Object
      val sc = new SparkContext(conf)
      
      //Creating sqlContext to convert List values into Data Frame Objects using SQL Context Implicits
      val sqlContext= new org.apache.spark.sql.SQLContext(sc)
      import sqlContext.implicits._
      
      //Reading InputFile Name from class First argument
      val fileInput = args(0)
      var strLat = args(2)
      var strLong = args(3)
      val x_pos_in_file = strLat.substring(strLat.indexOf("=")+1).toInt
      val y_pos_in_file = strLong.substring(strLong.indexOf("=")+1).toInt
      
      val outputFileHeader = "x_cord,y_cord,LocationEntropy"

      //Creating RDD Object with textFile of SparkContext Class
      val myRDD = sc.textFile(fileInput)
      
      //Reading Header Record from the CSV File to filter at later stage
      val header = myRDD.first()
      //Creating List Buffer Object to store the output Computed
      var lstBuffer = mutable.ListBuffer.empty[String]
      
      //Filtering Header, Splitting each Row by "," and casting the values to Double data type
      var arr = myRDD.filter(r=>r != header).map(_.split(",").map(_.trim).toArray).collect()

      //Iterating the each value to call Entropy Function
      for (i <- 0 to arr.size-1){
          var dbl_xval =  arr(i)(x_pos_in_file).toDouble
          var dbl_yval =  arr(i)(y_pos_in_file).toDouble
          var dbl_Entropy = computeLEntropy(dbl_xval , dbl_yval)
          lstBuffer += dbl_xval+","+dbl_yval+","+dbl_Entropy
      }
      //Writing the Computed Location Entropy Value into Output File Name send as a second argument to the file
      val fnlDF = sc.parallelize((lstBuffer)).toDF(outputFileHeader.replaceAll("\"", ""))
      fnlDF.write.format("csv")
          .option("header", "true")
          .mode("overwrite")
          .save(args(1))
          
 }
  def computeLEntropy(x: Double, y:Double) : Double = {
    var sum = math.abs(x) + math.abs(y)
    var p1 = math.abs(x)/sum
    var p2 = math.abs(y)/sum
    
    //Applying Entropy give formula here, log base 2 value is computed e.g log2(10) =  log(10)/log(2)
    var dblEntropy = (-p1)*(math.log(p1)/scala.math.log(2))-(p2)*(math.log(p2)/scala.math.log(2))
    return dblEntropy
  }
}