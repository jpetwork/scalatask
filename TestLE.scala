package Test.geospatial

import org.junit.Test
import org.junit.Assert._
import org.scalatest.Assertions._
import net.test.geospatial._
class TestLE {
    @Test
    def testComputeLEntropy(){
      assertEquals(0.89049,ComputeEntropy.computeLEntropy(4.0,9.0),0.0001)
      assertEquals(0.9176956,ComputeEntropy.computeLEntropy(-35.67514,-71.5429),0.0001)
      assertEquals(0.953421,ComputeEntropy.computeLEntropy(7.3697,12.3547),0.0001)
    }
    @Test
    def testZeroArgsComputeLEntropy(){
      assertEquals(Double.NaN,ComputeEntropy.computeLEntropy(0,0),0.01)
    }
}